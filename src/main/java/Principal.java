import java.util.Scanner;

class Principal {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String palabra;
        System.out.println("Ingrese el texto a evaluar");
        palabra = entrada.nextLine();
        Integer i=0;
        String palabraSinEspacios = palabra.replaceAll(" ","").toLowerCase();
        boolean palindromo = true;

        while (i < palabraSinEspacios.length() / 2) {
            if (palabraSinEspacios.charAt(i) != palabraSinEspacios.charAt(palabraSinEspacios.length() -1 -i)) {
                palindromo = false;
            }
            i++;
        }

        System.out.println(palindromo ? "Es palíndromo" :"No es palíndromo");
    }

}


